<?php 
namespace blog;
use Illuminate\Database\Eloquent\Model;
class Comments extends Model {
  //comments table in database
  protected $guarded = [];
  // user who has commented
  public function author()
  {
    return $this->belongsTo('blog\User','from_user');
  }
  // returns post of any comment
  public function post()
  {
    return $this->belongsTo('blog\Posts','on_post');
  }
}
