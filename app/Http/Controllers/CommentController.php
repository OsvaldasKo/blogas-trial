<?php

namespace blog\Http\Controllers;

use blog\Posts;
use blog\User;
use Redirect;
use Illuminate\Http\Request;
use blog\Http\Requests;
use blog\Http\Controllers\Controller;
use blog\Http\Requests\PostFormRequest;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        //on_post, from_user, body
    	$input['from_user'] = $request->user()->id;
        $input['on_post'] = $request->input('on_post');
        $input['body'] = $request->input('body');
        $slug = $request->input('slug');
        Comments::create($input);
        return redirect($slug)->with('message', 'Comment published');
    }
}
